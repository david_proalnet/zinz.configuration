﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace Zinz.Configuration
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private static string encodeString(string inputString)
        {
            byte[] stringBytes = System.Text.Encoding.UTF8.GetBytes(inputString);
            string encoded = Convert.ToBase64String(stringBytes);
            arrangeEncodedString(ref encoded);
            return encoded;
        }

        private static string decodeString(string inputString)
        {
            arrangeEncodedString(ref inputString);
            byte[] stringBytes = Convert.FromBase64String(inputString);
            return System.Text.Encoding.UTF8.GetString(stringBytes);
        }

        private static void arrangeEncodedString(ref string inputString)
        {
            char secondChar = inputString[1];
            char secondToLastChar = inputString[inputString.Length - 2];
            inputString = inputString.Remove(1, 1);
            inputString = inputString.Insert(1, secondToLastChar.ToString());
            inputString = inputString.Remove(inputString.Length - 2, 1);
            inputString = inputString.Insert(inputString.Length - 1, secondChar.ToString());
        }

        private void chkConfigProxy_CheckedChanged(object sender, EventArgs e)
        {
            ChangeReadOnlyProperty(!chkConfigProxy.Checked);
        }

        private void ChangeReadOnlyProperty(Boolean Status)
        {
            txProxyUri.ReadOnly = Status;
            txUserProxy.ReadOnly = Status;
            txPasswordProxy.ReadOnly = Status;
        }

        private void LimpiarCampos()
        {
            txLlave.Text = String.Empty;
            txUser.Text = String.Empty;
            txPassword.Text = String.Empty;
            txCompanyId.Text = String.Empty;
            txCompanyName.Text = String.Empty;
            txProxyUri.Text = String.Empty;
            txUserProxy.Text = String.Empty;
            txPasswordProxy.Text = String.Empty;
            chkConfigProxy.Checked = false;
            ChangeReadOnlyProperty(true);
        }

        private String ValidarCampos()
        {
            StringBuilder sb = new StringBuilder();
            if(String.IsNullOrEmpty(txLlave.Text.Trim()))
            {
                sb.Append("Debe ingresar una llave de instalación");
            }

            if (String.IsNullOrEmpty(txUser.Text.Trim()))
            {
                sb.Append("Debe ingresar un usuario");
            }

            if (String.IsNullOrEmpty(txPassword.Text.Trim()))
            {
                sb.Append("Debe ingresar una contraseña");
            }

            if (String.IsNullOrEmpty(txCompanyName.Text.Trim()))
            {
                sb.Append("Debe ingresar un nombre de compañía");
            }

            if (String.IsNullOrEmpty(txCompanyId.Text.Trim()))
            {
                sb.Append("Debe ingresar un código de compañía");
            }
            else
            {
                int CompanyId = -1;
                if (!int.TryParse(txCompanyId.Text.Trim(), out CompanyId))
                {
                    sb.Append("El código de compañía debe ser númerico");
                }
            }

            if (chkConfigProxy.Checked)
            {
                if (String.IsNullOrEmpty(txProxyUri.Text.Trim()))
                {
                    sb.Append("Debe ingresar una url de proxy");
                }

                if (String.IsNullOrEmpty(txUserProxy.Text.Trim()))
                {
                    sb.Append("Debe ingresar un usuario de proxy");
                }

                if (String.IsNullOrEmpty(txPasswordProxy.Text.Trim()))
                {
                    sb.Append("Debe ingresar una contraseña de proxy");
                }
            }

            return sb.ToString();

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            String msjError = ValidarCampos();
            if (!String.IsNullOrEmpty(msjError))
            {
                MessageBox.Show(msjError, "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }else
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(encodeString(txLlave.Text));
                sb.Append("¬");
                sb.Append(encodeString(txUser.Text));
                sb.Append("¬");
                sb.Append(encodeString(txPassword.Text));
                sb.Append("¬");
                sb.Append(encodeString(txCompanyName.Text));
                sb.Append("¬");
                sb.Append(txCompanyId.Text);
                sb.Append("¬");

                if (chkConfigProxy.Checked)
                {
                    sb.Append(txProxyUri.Text);
                    sb.Append("¬");
                    sb.Append(txUserProxy.Text);
                    sb.Append("¬");
                    sb.Append(encodeString(txPasswordProxy.Text));
                    sb.Append("¬");
                }

                SaveFileDialog sfd = new SaveFileDialog();

                sfd.Filter = "Text|*.txt";
                sfd.Title = "Guardar Archivo de Configuración";
                sfd.CheckPathExists = true;

                if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    if (!String.IsNullOrEmpty(sfd.FileName))
                    {
                        String fn = Path.GetFileName(sfd.FileName);
                        fn = sfd.FileName.Replace(fn, "ZinzConfiguration.txt");

                        if (File.Exists(fn))
                        {
                            File.Delete(fn);
                        }
                        StreamWriter sw = new StreamWriter(fn);
                        sw.Write(sb.ToString());
                        sw.Close();
                        sw.Dispose();
                        MessageBox.Show("Archivo generado satisfactoriamente", "Información", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LimpiarCampos();
                    }
                }
            }
        }
    }
}
