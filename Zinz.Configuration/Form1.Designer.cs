﻿namespace Zinz.Configuration
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txLlave = new System.Windows.Forms.TextBox();
            this.txUser = new System.Windows.Forms.TextBox();
            this.txPassword = new System.Windows.Forms.TextBox();
            this.txCompanyName = new System.Windows.Forms.TextBox();
            this.txCompanyId = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkConfigProxy = new System.Windows.Forms.CheckBox();
            this.txProxyUri = new System.Windows.Forms.TextBox();
            this.txPasswordProxy = new System.Windows.Forms.TextBox();
            this.txUserProxy = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Llave de Instalación";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 61);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Usuario";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(17, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(61, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Contraseña";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(374, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Nombre Compañía";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(374, 61);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Código Compañía";
            // 
            // txLlave
            // 
            this.txLlave.Location = new System.Drawing.Point(134, 27);
            this.txLlave.Name = "txLlave";
            this.txLlave.Size = new System.Drawing.Size(208, 20);
            this.txLlave.TabIndex = 1;
            // 
            // txUser
            // 
            this.txUser.Location = new System.Drawing.Point(134, 58);
            this.txUser.Name = "txUser";
            this.txUser.Size = new System.Drawing.Size(208, 20);
            this.txUser.TabIndex = 1;
            // 
            // txPassword
            // 
            this.txPassword.Location = new System.Drawing.Point(134, 91);
            this.txPassword.Name = "txPassword";
            this.txPassword.Size = new System.Drawing.Size(208, 20);
            this.txPassword.TabIndex = 1;
            // 
            // txCompanyName
            // 
            this.txCompanyName.Location = new System.Drawing.Point(485, 27);
            this.txCompanyName.Name = "txCompanyName";
            this.txCompanyName.Size = new System.Drawing.Size(208, 20);
            this.txCompanyName.TabIndex = 1;
            // 
            // txCompanyId
            // 
            this.txCompanyId.Location = new System.Drawing.Point(485, 58);
            this.txCompanyId.Name = "txCompanyId";
            this.txCompanyId.Size = new System.Drawing.Size(208, 20);
            this.txCompanyId.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txPassword);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.txCompanyId);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txUser);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txCompanyName);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txLlave);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(712, 141);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos Generales";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.chkConfigProxy);
            this.groupBox2.Controls.Add(this.txUserProxy);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txProxyUri);
            this.groupBox2.Controls.Add(this.txPasswordProxy);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Location = new System.Drawing.Point(13, 174);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(711, 123);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Datos de Proxy";
            // 
            // chkConfigProxy
            // 
            this.chkConfigProxy.AutoSize = true;
            this.chkConfigProxy.Location = new System.Drawing.Point(7, 20);
            this.chkConfigProxy.Name = "chkConfigProxy";
            this.chkConfigProxy.Size = new System.Drawing.Size(168, 17);
            this.chkConfigProxy.TabIndex = 0;
            this.chkConfigProxy.Text = "Configurar Conexión por Proxy";
            this.chkConfigProxy.UseVisualStyleBackColor = true;
            this.chkConfigProxy.CheckedChanged += new System.EventHandler(this.chkConfigProxy_CheckedChanged);
            // 
            // txProxyUri
            // 
            this.txProxyUri.Location = new System.Drawing.Point(133, 49);
            this.txProxyUri.Name = "txProxyUri";
            this.txProxyUri.ReadOnly = true;
            this.txProxyUri.Size = new System.Drawing.Size(208, 20);
            this.txProxyUri.TabIndex = 1;
            // 
            // txPasswordProxy
            // 
            this.txPasswordProxy.Location = new System.Drawing.Point(484, 49);
            this.txPasswordProxy.Name = "txPasswordProxy";
            this.txPasswordProxy.ReadOnly = true;
            this.txPasswordProxy.Size = new System.Drawing.Size(208, 20);
            this.txPasswordProxy.TabIndex = 1;
            // 
            // txUserProxy
            // 
            this.txUserProxy.Location = new System.Drawing.Point(133, 80);
            this.txUserProxy.Name = "txUserProxy";
            this.txUserProxy.ReadOnly = true;
            this.txUserProxy.Size = new System.Drawing.Size(208, 20);
            this.txUserProxy.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 83);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(43, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Usuario";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(373, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Contraseña";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 52);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Dirección Proxy";
            // 
            // btnGuardar
            // 
            this.btnGuardar.Location = new System.Drawing.Point(317, 312);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(103, 23);
            this.btnGuardar.TabIndex = 4;
            this.btnGuardar.Text = "Guardar Archivo";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(736, 342);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txLlave;
        private System.Windows.Forms.TextBox txUser;
        private System.Windows.Forms.TextBox txPassword;
        private System.Windows.Forms.TextBox txCompanyName;
        private System.Windows.Forms.TextBox txCompanyId;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox chkConfigProxy;
        private System.Windows.Forms.TextBox txUserProxy;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txProxyUri;
        private System.Windows.Forms.TextBox txPasswordProxy;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnGuardar;
    }
}

